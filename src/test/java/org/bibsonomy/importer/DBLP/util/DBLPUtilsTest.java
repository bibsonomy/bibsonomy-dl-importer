package org.bibsonomy.importer.DBLP.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * 
 * tests for {@link DBLPUtils}
 *
 * @author dzo
 */
public class DBLPUtilsTest {
	
	/**
	 * tests {@link DBLPUtils#normPersonName(String)}
	 * @throws Exception
	 */
	@Test
	public void testNormPersonName() throws Exception {
		assertNull(DBLPUtils.normPersonName(null));
		assertEquals("Yang Wang", DBLPUtils.normPersonName("Yang Wang 0007"));
		assertEquals("Yang 12 Wang", DBLPUtils.normPersonName("Yang 12 Wang 0007"));
		assertEquals("Yang 0007 Wang", DBLPUtils.normPersonName("Yang 0007 Wang"));
	}
}
