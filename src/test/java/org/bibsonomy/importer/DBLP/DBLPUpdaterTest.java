package org.bibsonomy.importer.DBLP;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.bibsonomy.common.Pair;
import org.bibsonomy.util.Sets;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.regex.Matcher;

/**
 * @author rja
 */
public class DBLPUpdaterTest {

	private static final String BASE_URL = "https://www.bibsonomy.org/";

	@Test
	@Ignore // remove test and also requires a secret
	public void testOpenSession() throws IOException {
		final String cookie = "dummy";

		final Pair<String, String> info = DBLPUpdater.retrieveInformation(BASE_URL, "dblp", "db_user=" + cookie + ";");

		assertNotNull(info.getFirst());
	}

	@Test
	public void testUserFindPattern() {
		final Matcher matcher = DBLPUpdater.USER_PATTERN.matcher("var currUser = \"dblp\";");
		assertTrue(matcher.matches());
		assertEquals("dblp", matcher.group(1));
	}

	@Test
	@Ignore // remote test
	public void testGetCookie() throws IOException {
		final HttpURLConnection connection = DBLPUpdater.buildConnection(BASE_URL, Collections.<String>emptySet());
		connection.connect();
		final String jsessionid = DBLPUpdater.getCookie(connection, "JSESSIONID");
		assertNotNull(jsessionid);
	}

}