#!/bin/sh

#
# Start the DBLP update
#
# Changes:
# 2014-01-14 (rja)
# - increased entityExpansionLimit to 200k since we got the error "org.xml.sax.SAXParseException; lineNumber: 1; columnNumber: 1; JAXP00010001: The parser has encountered mo$



DIR=/home/bibsonomy/DBLP

cd $DIR

# put JARs in lib/ into classpath
CLASSPATH=.
for jar in lib/*.jar; do
  CLASSPATH=$CLASSPATH:$jar
done

# see https://community.oracle.com/thread/2594170
# and http://vaguehope.com/2013/10/jdk-1-7-0_45-xml-and-jaxp00010001/
# and http://docs.oracle.com/javase/1.5.0/docs/guide/xml/jaxp/JAXP-Compatibility_150.html#JAXP_security
# to understand -DentityExpansionLimit=100000

# run DBLP update
java -Xmx10000M -DentityExpansionLimit=0 -cp $CLASSPATH org.bibsonomy.importer.DBLP.DBLPUpdater $*