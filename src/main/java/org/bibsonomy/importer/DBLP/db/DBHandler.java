package org.bibsonomy.importer.DBLP.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.bibsonomy.common.enums.HashID;
import org.bibsonomy.importer.DBLP.DBLPEvaluation;
import org.bibsonomy.importer.DBLP.configuration.Configuration;

/**
 * The DBHandler gives Methods to get the hash-value of a given bibtex-key.
 * It needs the values of the DBLPConstants.xml.
 */
public class DBHandler{
	private Configuration configuration = null;

	private Connection conn = null;
	
	private PreparedStatement stmtp = null;
	
	private ResultSet rst = null;
	
	private static final String SQL_GET_BIBKEYS = "SELECT bibtexKey FROM bibtex WHERE user_name=?";
	
	private static final String SQL_GET_BOOKHASH = "SELECT book_url_hash FROM bookmark WHERE LEFT(book_extended,?)=? and user_name=?";
	
	private static final String SQL_GET_BIBKEYS_BOOKMARK = "SELECT book_extended FROM bookmark WHERE user_name=?";	
	
	private static final String SQL_GET_BIBHASH = "SELECT simhash" + HashID.INTRA_HASH.getId() + " FROM bibtex WHERE user_name=? AND bibtexKey=?";
	
	private static final String SQL_GET_LAST_UPDATE = "SELECT lastupdate FROM DBLP ORDER BY lastupdate DESC LIMIT 1";
	
	private static final String SQL_SET_LAST_UPDATE = "INSERT INTO DBLP (`lastupdate`) VALUES (?)";
	
	public DBHandler(final Configuration conResult) throws Exception{
		this.configuration = conResult;
		if (!conResult.isValid() || conResult == null) {
			throw new Exception("configuration is invalid");
		}
	}
	
	public void open() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		String url = "jdbc:mysql://" + this.configuration.getDbhost() + "/" + this.configuration.getDbname() + "?autoReconnect=true&amp;useUnicode=true&amp;characterEncoding=utf-8&amp;mysqlEncoding=utf8";
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		this.conn = DriverManager.getConnection(url, this.configuration.getDbuser(), this.configuration.getDbpassword());
	}

	public void close() {
		try {
			if (this.rst != null) {
				this.rst.close();
			}
		} catch (final SQLException e) {
			// ignore
		}
		this.rst   = null;

		try {
			if (this.stmtp != null) {
				this.stmtp.close();
			}
		} catch (final SQLException e) {
			// ignore
		}
		this.stmtp = null;

		try {
			if (this.conn  != null) {
				conn.close();
			}
		} catch (final SQLException e) {
			// ignore
		}

		this.conn = null;
	}

	/**
	 *
	 * @return all bibtex keys of the configured import user
	 * @throws SQLException
	 */
	public Set<String> getBibKeysBibtex() throws SQLException{
		final Set<String> keys = new HashSet<>();
		if (this.conn != null) {
			this.stmtp = this.conn.prepareStatement(SQL_GET_BIBKEYS);
			this.stmtp.setString(1, this.configuration.getUser());
			this.rst = this.stmtp.executeQuery();
			
			while (rst.next()) {
				keys.add(rst.getString("bibtexKey"));
			}
		}
		return keys;
	}

	/**
	 *
	 * @return all bookmark-keys from the configured import user
	 * @throws SQLException
	 */
	public Set<String> getBibKeysBookmark() throws SQLException {
		final Set<String> keys = new HashSet<>();
		if (this.conn != null) {
			this.stmtp = this.conn.prepareStatement(SQL_GET_BIBKEYS_BOOKMARK);
			this.stmtp.setString(1, this.configuration.getUser());
			this.rst = this.stmtp.executeQuery();
			while (this.rst.next()) {
				final String extended = rst.getString("book_extended");
				final int indexOfComma = extended.indexOf(",");
				if (indexOfComma > 0) {
					keys.add(extended.substring(0, indexOfComma));
				}
			}
		}
		return keys;
	}
	
	/*
	 * returns the date of the last DBLP update
	 */
	public Date getDBLPDate() throws Exception {
		if (this.conn != null) {
			this.stmtp = this.conn.prepareStatement(SQL_GET_LAST_UPDATE);
			this.rst = this.stmtp.executeQuery();

			if (this.rst.next()) {
				return this.rst.getDate("lastupdate");
			}
			
			// return a date before the project start of DBLP
			// I guess 1970 should be ok (dzo)
			return new Date(0);
		} else {
			throw new Exception("Cannot connect to database server");
		}
	}

	/*
	 * set the date of the last DBLP update
	 */
	public void setDBLPDate(Date dblpdate) throws Exception{
		if (this.conn != null) {
			this.stmtp = this.conn.prepareStatement(SQL_SET_LAST_UPDATE);
			this.stmtp.setDate(1, new java.sql.Date(dblpdate.getTime()));
			this.stmtp.executeUpdate();
		} else {
			throw new Exception("Cannot connect to database server");
		}
	}

	/**
	 *
	 * @param bibKey the key of the bookmark
	 * @return the hash from the bookmark with a given bibtexKey(in book_extended row) from
	 * specified import user
	 * @throws SQLException
	 */
	public String getUrlHash(String bibKey) throws SQLException{
		bibKey = bibKey + ",";
		if (this.conn != null) {
			this.stmtp = this.conn.prepareStatement(SQL_GET_BOOKHASH);
			this.stmtp.setInt(1, bibKey.length());
			this.stmtp.setString(2, bibKey);
			this.stmtp.setString(3, this.configuration.getUser());
			this.rst = this.stmtp.executeQuery();
			
			if (this.rst.next()) {
				return this.rst.getString("book_url_hash");
			}
		} else {
			throw new SQLException("Cannot connect to database server");
		}
		return null;
	}

	/**
	 *
	 * returns hash from a publ with a given bibtexKey from the
	 * specified import user
	 *
	 * @param bibkey
	 * @return
	 * @throws SQLException
	 */
	public String getBibhash(String bibkey) throws SQLException {
		if (this.conn != null) {
			this.stmtp = this.conn.prepareStatement(SQL_GET_BIBHASH);
			this.stmtp.setString(1, this.configuration.getUser());
			this.stmtp.setString(2, bibkey);
			this.rst = this.stmtp.executeQuery();
			
			if (this.rst.next()) {
				return this.rst.getString("simhash" + HashID.INTRA_HASH.getId());
			}
		} else {
			throw new SQLException("Cannot connect to database server");
		}

		return null;
	}
}