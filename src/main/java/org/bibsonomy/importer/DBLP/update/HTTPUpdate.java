package org.bibsonomy.importer.DBLP.update;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.importer.DBLP.DBLPException;


/**
 * @author rja
 */
public abstract class HTTPUpdate {
	private static final Log log = LogFactory.getLog(HTTPUpdate.class);

	protected final String baseURL;
	private final String userCookie;

	protected final String cKey;
	private final String sessionCookie;

	private final String userName;
	
	
	public HTTPUpdate(final String baseURL, final String userCookie, final String sessionCookie, final String cKey, final String userName) {
		this.baseURL = baseURL;
		this.userCookie = userCookie;
		this.cKey = cKey;
		this.sessionCookie = sessionCookie;

		this.userName = userName;
	}

	/**
	 * Calls the specified URL and throws an exception if the return code signals an error.
	 *  
	 * @param url
	 * @throws IOException
	 * @throws DBLPException
	 */
	protected void callURL(final URL url) throws IOException, DBLPException {
		log.debug("calling " + url);
		
		final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		setCookies(conn);
		conn.setInstanceFollowRedirects(false); // do not follow redirects!

		conn.getContentLength();

		final int response = conn.getResponseCode();
		conn.disconnect();
		if (!(response > 0 && response < 400)) {
			throw new DBLPException("got " + response + " when calling " + url);
		}
	}

	/**
	 * deletes the resource with the given hash
	 * @param hash
	 * @throws IOException
	 * @throws DBLPException
	 */
	protected void deletePost(String hash) throws IOException, DBLPException {
		callURL(new URL(this.baseURL + "deletePost?owner=" + this.userName + "&resourceHash=" + hash + "&ckey="+ this.cKey));
	}
	
	protected void setCookies(final HttpURLConnection conn) {
		conn.setRequestProperty("Cookie",  this.userCookie + ";" + this.sessionCookie);
	}
}
