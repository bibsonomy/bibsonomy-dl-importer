package org.bibsonomy.importer.DBLP.update;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.importer.DBLP.db.DBHandler;


public class CheckDeletedEntries {

	private static final Log log = LogFactory.getLog(CheckDeletedEntries.class);
	
	private Set<String> bookmarkKeysInDatabase;
	private Set<String> bibtexKeysInDatabase;

	private final HTTPBookmarkUpdate bookUpdate;
	private final HTTPBibtexUpdate bibUpdate;
	
	public CheckDeletedEntries(HTTPBookmarkUpdate bookUpdate, HTTPBibtexUpdate bibUpdate) {
		this.bookUpdate = bookUpdate;
		this.bibUpdate = bibUpdate;
	}

	public void deleteOldPosts(final Set<String> allKeys, final DBHandler handler) {
		log.info("looking for old posts");
		
		try {
			/*
			 * get all keys from BibSonomy
			 */
			handler.open();
			bookmarkKeysInDatabase = handler.getBibKeysBookmark();
			bibtexKeysInDatabase = handler.getBibKeysBibtex();
			handler.close();

			final List<String> bookmarksToDelete = new LinkedList<>();
			final List<String> bibtexsToDelete   = new LinkedList<>();

			/*
			 * collect all keys which are not contained in the dataset any more
			 */
			for (final String key : bookmarkKeysInDatabase) {
				if (!allKeys.contains(key)) {
					bookmarksToDelete.add(key);
				}
			}

			for (final String key : bibtexKeysInDatabase) {
				if (!allKeys.contains(key))	{
					bibtexsToDelete.add(key);
				}
			}

			log.info("Statistics: ");
			log.info("            #allKeys = " + allKeys.size() + 
					           ", #delBook = " + bookmarksToDelete.size() +
					           ", #delBibt = " + bibtexsToDelete.size());
			log.info("            #dbBook = " + bookmarkKeysInDatabase.size() + ", #dbBib = " + bibtexKeysInDatabase.size());
			
			
			bookUpdate.deleteOldBookmarkByKey(bookmarksToDelete, handler);
			bibUpdate.deleteOldBibtexByKey(bibtexsToDelete, handler);
			
		} catch(final Exception e) {
			log.fatal(e);
			handler.close();
		}
	}

	public Set<String> getBibtexKeysInDatabase() {
		return bibtexKeysInDatabase;
	}

	public Set<String> getBookmarkKeysInDatabase() {
		return bookmarkKeysInDatabase;
	}

}