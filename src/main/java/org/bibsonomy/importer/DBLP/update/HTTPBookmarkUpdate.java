package org.bibsonomy.importer.DBLP.update;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.importer.DBLP.DBLPException;
import org.bibsonomy.importer.DBLP.db.DBHandler;
import org.bibsonomy.importer.DBLP.parser.DBLPEntry;
import org.bibsonomy.importer.DBLP.parser.DBLPParseResult;
import org.bibsonomy.util.StringUtils;


/**
 * This class deletes all old DBLP entries which will be updated
 */
public class HTTPBookmarkUpdate extends HTTPUpdate {

	private static final Log log = LogFactory.getLog(HTTPBookmarkUpdate.class);

	private static final String dblpHomeUrlHash = StringUtils.getMD5Hash(DBLPEntry.DBLPURL);

	private static final SimpleDateFormat ISO8601_FORMAT_HELPER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");


	public HTTPBookmarkUpdate(final String baseURL, final String userCookie, final String sessionCookie, final String cKey, final String userName) {
		super(baseURL, userCookie, sessionCookie, cKey, userName);
	}

	/*
	 * iterate through all parsed entries and then delete them with delete()
	 */
	public void deleteOldBookmarkByEntry(final List<DBLPEntry> list, final DBHandler handler, final Set<String> db_bookmark_keys) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, DBLPException {
		handler.open(); 

		for (final DBLPEntry entry : list) {
			if (db_bookmark_keys.contains(entry.getDblpKey())) {
				final String urlhash = handler.getUrlHash(entry.getDblpKey());
				if (urlhash != null) {
					try {
						deletePost(urlhash);
					} catch (final IOException e) {
						log.error("error while deleting bookmark " + urlhash, e);
					}
				}
			}
		}
		handler.close();
	}

	/** Updates the URL of DBLP.
	 *  
	 * @throws DBLPException
	 */
	public void updateDBLPHome() throws DBLPException {
		log.debug("updating DBLP url");
		try {
			deletePost(dblpHomeUrlHash);
		} catch (final IOException e) {
			log.fatal("could not delete DBLP url", e);
		}

		try {
			insertPost(DBLPEntry.DBLPURL, "DBLP Computer Science Bibliography", "", null);
		} catch (final IOException e) {
			log.fatal("could not insert DBLP url", e);
		}
	}

	/**
	 * iterate through all keys and delete
	 */
	public void deleteOldBookmarkByKey(final List<String> keys, final DBHandler handler) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, DBLPException {
		log.info("deleting bookmarks");
		handler.open();
		for (final String key: keys) {
			final String hash = handler.getUrlHash(key);
			
			log.debug("looking at hash " + hash);
			
			if (hash != null) {
				try {
					/*
					 * delete post
					 */
					deletePost(hash);
				} catch (IOException e) {
					log.fatal("tried to delete bookmark " + hash + ": ", e);
				}
			} else {
				log.debug("did not delete, hash == null");
			}
		}
		handler.close();
	}
	
	/*
	 * inserts the given bookmark
	 */
	private void insertPost(String url, final String title, final String description, final Date date) throws IOException, DBLPException {
		if (url.startsWith("db/")) {
			url = DBLPEntry.DBLPURL + url;
		}
		
		final HttpURLConnection conn = (HttpURLConnection) new URL(this.baseURL + "editBookmark").openConnection();
		setCookies(conn);
		conn.setDoOutput(true); // neccessary to write to connection
		conn.setDoInput(true);  // defaults to true ... but just to be sure ...		
		conn.setInstanceFollowRedirects(false); // do not follow redirects!
		
		/* write request parameters */
		final PrintWriter out = new PrintWriter(conn.getOutputStream());
		out.print("url=" + URLEncoder.encode(url, "UTF-8"));
		out.print("&description=" + URLEncoder.encode(title, "UTF-8"));
		out.print("&extended=" + URLEncoder.encode(description, "UTF-8"));
		out.print("&tags=" + URLEncoder.encode("dblp", "UTF-8"));
		out.print("&ckey=" + this.cKey);
		if (date != null) out.print("&date=" + URLEncoder.encode(ISO8601_FORMAT_HELPER.format(date), "UTF-8"));
		out.close();
		
		conn.getContentLength(); // actually connect
		
		final int response = conn.getResponseCode();
		conn.disconnect();
		
		if (!(response > 0 && response < 400)) {
			throw new DBLPException("got " + response + " when trying to inserting bookmark " + url);
		}
	}

	/**
	 * insert all bookmarks from entrylist
	 * @param entrylist the entrylist to extract the bookmarks from
	 * @param presult
	 * @throws DBLPException
	 */
	public void insertNewBookmark(final List<DBLPEntry> entrylist, DBLPParseResult presult) throws DBLPException {
		for (final DBLPEntry entry : entrylist) {
			try {
				if (entry.getUrl() != null) {
					/*
					 * entry has URL - use it for bookmark
					 */
					presult.getEval().incrementUpdate(entry);
					/*
					 * clean title to contain author name
					 */
					String title = entry.getTitle();
					final String author = entry.getAuthor();
					if ("Home Page".equals(title) && author != null) {
						title += " of " + author;
					}
					/*
					 * insert post
					 */
					insertPost(entry.getUrl(), title, entry.generateExtended(), entry.getEntrydate());
				} else if (entry.getEe() != null) {
					/*
					 * entry has EE (electronic edition) entry - use it to generate a URL 
					 */
					presult.getEval().incrementUpdate(entry);
					insertPost(DBLPEntry.DBLPURL + entry.getEe(), entry.getTitle(), entry.generateExtended(), entry.getEntrydate());
				} else {
					/*
					 * no URL found - don't insert entry
					 */
					presult.getEval().incrementUpdate(entry);
					presult.getInsert_bookmark_empty_url().add(entry);
				}
			} catch (final IOException e) {
				log.fatal("BookmarkInsert: ", e);
			}
		}
	}
}