package org.bibsonomy.importer.DBLP.update;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.importer.DBLP.db.DBHandler;
import org.bibsonomy.importer.DBLP.parser.DBLPEntry;
import org.bibsonomy.importer.DBLP.parser.DBLPParseResult;


public class BibtexUpdate {

	private static final Log log = LogFactory.getLog(BibtexUpdate.class);

	private static final int maxNumberOfEntries = 100;

	private final HTTPBibtexUpdate httpBibUpdate;
	private final DBLPParseResult presult;
	private final DBHandler handler;

	public BibtexUpdate(HTTPBibtexUpdate httpBibUpdate, DBLPParseResult presult, DBHandler handler) {
		this.presult = presult;
		this.handler = handler;
		this.httpBibUpdate = httpBibUpdate;
	}

	/*
	 * this method insert 10 entries from the same type in a row
	 */
	public int update(final Set<String> bibtexKeysInDatabase) {
		int updateCount = 0;
		log.info("updating article");
		updateCount += handleEntries(bibtexKeysInDatabase, this.presult.getArticles());
		log.info("updating book");
		updateCount += handleEntries(bibtexKeysInDatabase, this.presult.getBooks());
		log.info("updating incollection");
		updateCount += handleEntries(bibtexKeysInDatabase, this.presult.getIncollections());
		log.info("updating inproceeding");
		updateCount += handleEntries(bibtexKeysInDatabase, this.presult.getInproceedings());
		log.info("updating masterthesis");
		updateCount += handleEntries(bibtexKeysInDatabase, this.presult.getMastersthesis());
		log.info("updating phdthesis");
		updateCount += handleEntries(bibtexKeysInDatabase, this.presult.getPhdthesis());
		log.info("updating proceeding");
		updateCount += handleEntries(bibtexKeysInDatabase, this.presult.getProceedings());
		log.info("updating data");
		updateCount += handleEntries(bibtexKeysInDatabase, this.presult.getData());
		log.info("updating crossrefs");
		updateCount += handleCrossRefs(bibtexKeysInDatabase);
		
		log.info("HTTP status codes: " + this.httpBibUpdate.getHttpStatusCounts());
		return updateCount;
	}


	private int handleCrossRefs(Set<String> db_bibtex_keys) {
		final LinkedList<DBLPEntry> list = new LinkedList<>();
		int error_count = 0;
		int listCounter = 0;
		int nullCounter = 0;
		int notNullCounter = 0;

		// iterate over all reference entries
		for (final String keyReference : presult.getCrossrefEntries().keySet()) {
			final Map<String, DBLPEntry> crossrefEntries = presult.getCrossrefEntries().get(keyReference);
			if (crossrefEntries != null) { // check if crossref is already updated
				// iterate over all crossref entry from a reference entry
				for (final DBLPEntry entry : crossrefEntries.values()){
					listCounter++;
					if (entry.getEntrydate().after(presult.getDblpdate())) {
						list.add(entry);
					}
				}
				if (list.size() > 0) {
					for (DBLPEntry entry : presult.getCrossreflist()) {
						if (entry.getDblpKey().equals(keyReference)) {
							list.add(entry);
							break;
						}
					}

					deleteAndInsertEntries(db_bibtex_keys, list);
					list.clear();
				}
				notNullCounter++;
			} else {
				nullCounter++;
			}
		}

		return error_count;
	}


	/*
	 * handles entries for one entry type 
	 */
	private int handleEntries(final Set<String> bibtexKeysInDatabase, final List<DBLPEntry> entries) {
		/*
		 * contains the entries that will be updated
		 */
		final LinkedList<DBLPEntry> updateList = new LinkedList<>();
		int updateCount = 0;

		log.info("   handling " + entries.size() + " entries");
		/*
		 * iterate over all entries in given list
		 */
		for (final DBLPEntry entry : entries) {
			if (entry.getEntrydate().after(presult.getDblpdate())){
				/*
				 * entry is newer than last insert date --> add it to update list
				 */
				updateList.add(entry);

				/*
				 * insert crossref entries
				 */
				if (presult.getCrossrefEntries().containsKey(entry.getDblpKey())) {
					presult.getCrossreflist().add(entry);
					final Map<String, DBLPEntry> crossrefs = presult.getCrossrefEntries().get(entry.getDblpKey());
					if (crossrefs != null) {
						updateList.addAll(crossrefs.values());
						presult.getCrossrefEntries().put(entry.getDblpKey(), null);
					}
				}
			} else if (presult.getCrossrefEntries().containsKey(entry.getDblpKey())){
				presult.getCrossreflist().add(entry);
			}

			if (updateList.size() >= maxNumberOfEntries) {
				updateCount += updateList.size();
				deleteAndInsertEntries(bibtexKeysInDatabase, updateList);
				updateList.clear();
			}
		}
		updateCount += updateList.size();

		if (updateList.size() > 1) { //store the rest
			deleteAndInsertEntries(bibtexKeysInDatabase, updateList);
		} else if (updateList.size() == 1) {
			// FIXME: dirty hack: bibsonomy does not insert one entry alone by it self
			updateList.add(updateList.getFirst());
			deleteAndInsertEntries(bibtexKeysInDatabase, updateList);
		}
		entries.clear();
		log.info("   updated " + updateCount + " entries");
		return updateCount;

	}

	/**
	 * deletes entries from list (their old counterparts) and adds new version
	 * @param bibTeXKeys
	 * @param list
	 */
	private void deleteAndInsertEntries(final Set<String> bibTeXKeys, final List<DBLPEntry> list) {
		try {
			// delete
			this.httpBibUpdate.deleteOldBibtexByEntry(list, this.handler, bibTeXKeys);

			// insert
			this.httpBibUpdate.insertNewBibtex(list, this.presult.getEval());
		} catch(final Exception e) {
			/*
			 * error handling
			 */
			log.fatal("error while updating entries", e);
			this.handler.close();
		}
	}
}