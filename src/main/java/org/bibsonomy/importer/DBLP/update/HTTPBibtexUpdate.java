package org.bibsonomy.importer.DBLP.update;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.importer.DBLP.DBLPEvaluation;
import org.bibsonomy.importer.DBLP.DBLPException;
import org.bibsonomy.importer.DBLP.db.DBHandler;
import org.bibsonomy.importer.DBLP.parser.DBLPEntry;


/**
 * This class delete all old DBLP entries which will be updated
 */
public class HTTPBibtexUpdate extends HTTPUpdate {
	private static final Log log = LogFactory.getLog(HTTPBibtexUpdate.class);


	private final Map<String, Integer> httpStatusCounts = new HashMap<>();

	public HTTPBibtexUpdate(final String baseURL, final String userCookie, final String sessionCookie, final String cKey, final String userName) {
		super(baseURL, userCookie, sessionCookie, cKey, userName);
	}

	/**
	 * iterate through all parsed entries and then delete them with delete()
	 * @param presult
	 * @param handler
	 * @param db_bibtex_keys
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws DBLPException
	 */
	public void deleteOldBibtexByEntry(final List<DBLPEntry> presult, final DBHandler handler, final Set<String> db_bibtex_keys) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, DBLPException{
		handler.open();
		for (final DBLPEntry entry: presult) {
			final String dblpKey = entry.getDblpKey();

			log.debug("looking at " + dblpKey);

			if (db_bibtex_keys.contains(dblpKey)) { // delete only if entry already exist in DB
				log.debug("is in DB -> delete it");

				final String hash = handler.getBibhash(dblpKey);
				if (hash != null) {
					try {
						/*
						 * delete post
						 */
						deletePost(hash);
					} catch (final IOException e) {
						log.fatal("tried to delete publication " + hash + ": " + e);
					}
				} else {
					log.debug("hash == null, could not delete");
				}
			}
		}
		handler.close();
	}

	/*
	 * iterate through all keys and delete
	 */
	public void deleteOldBibtexByKey(final List<String> keys, DBHandler handler) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, DBLPException {
		log.info("deleting bibtex");
		handler.open();
		for (final String key : keys){
			final String hash = handler.getBibhash(key);

			log.debug("looking at hash " + hash);

			if (hash != null) {
				try {
					// delete post
					this.deletePost(hash);
				} catch (final IOException e) {
					log.fatal("tried to delete bibtex " + hash + ": ", e);
				}
			} else {
				log.debug("did not delete, hash == null");
			}
		}
		handler.close();
	}

	/*
	 * insert the given snippet
	 */
	private void uploadBibTeXSnippet(final String snippet) throws IOException {
		log.debug("inserting entry");

		final HttpURLConnection conn = (HttpURLConnection) new URL(this.baseURL + "import/publications?abstractGrouping=public&overwrite=true&editBeforeImport=false&selTab=1&ckey=" + this.cKey).openConnection();
		setCookies(conn);
		conn.setDoOutput(true); // neccessary to write to connection
		conn.setDoInput(true);  // defaults to true ... but just to be sure ...
		conn.setInstanceFollowRedirects(false); // do not follow redirects!
		conn.setRequestMethod("POST");

		/* write request parameters */
		final PrintWriter out = new PrintWriter(conn.getOutputStream());
		out.print("selection=" + URLEncoder.encode(snippet, "UTF-8"));
		out.close();

		try {
			/*
			 * parse response
			 */
			final int responseStatus = conn.getResponseCode();
			this.countResponseStatus(String.valueOf(responseStatus));
			if (!(responseStatus > 0 && responseStatus < 400)) {
				throw new DBLPException("got " + responseStatus + " when trying to insert BibTeX " + snippet);
			}
		} catch (final Exception e) {
			log.fatal("Could not parse response document", e);
		} finally {
			conn.disconnect();
		}
	}
	
	private void countResponseStatus(final String code) {
		this.httpStatusCounts.put(code, this.getHttpStatusCounts().getOrDefault(code, 0) + 1);
	}

	/**
	 * inserts all new bibtex
	 * @param presult
	 * @param eval
	 * @throws IOException
	 * @throws DBLPException
	 */
	public void insertNewBibtex(final List<DBLPEntry> presult, final DBLPEvaluation eval) throws IOException, DBLPException {
		final StringBuilder snippets = new StringBuilder();
		for (final DBLPEntry entry : presult) {
			if (entry.getDblpKey() != null) {
				eval.incrementUpdate(entry);
				snippets.append(entry.generateSnippet());
			}
		}

		final String bibTeXString = snippets.toString();
		log.debug(bibTeXString);
		uploadBibTeXSnippet(bibTeXString);
	}


	public Map<String, Integer> getHttpStatusCounts() {
		return this.httpStatusCounts;
	}


}