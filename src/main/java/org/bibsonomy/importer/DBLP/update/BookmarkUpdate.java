package org.bibsonomy.importer.DBLP.update;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.importer.DBLP.db.DBHandler;
import org.bibsonomy.importer.DBLP.parser.DBLPEntry;
import org.bibsonomy.importer.DBLP.parser.DBLPParseResult;


public class BookmarkUpdate {
	private final static Log log = LogFactory.getLog(BookmarkUpdate.class);

	private static final int MAX_NUMBER_OF_ENTRIES = 10;
	
	private final HTTPBookmarkUpdate httpBookUpdate;
	private final DBLPParseResult presult;
	private final DBHandler handler;
	
	public BookmarkUpdate(final HTTPBookmarkUpdate httpBookUpdate, final DBLPParseResult presult, final DBHandler handler) {
		this.presult = presult;
		this.handler = handler;
		this.httpBookUpdate = httpBookUpdate;
	}

	public int update(final Set<String> dbBookmarkKeys) {
		int count = 0;
		count += handleEntries(dbBookmarkKeys, this.presult.getWww());
		count += handleEntries(dbBookmarkKeys, this.presult.getInsert_incomplete_author_editor());
		return count;
	}

	private int handleEntries(final Set<String> dbBookmarkKeys, final List<DBLPEntry> entryList) {
		final LinkedList<DBLPEntry> list = new LinkedList<>();
		int count = 0;

		for (final DBLPEntry entry : entryList) {
			if (entry.getEntrydate().after(this.presult.getDblpdate())) {
				list.add(entry);

				if (list.size() >= MAX_NUMBER_OF_ENTRIES){
					count += list.size();
					try {
						this.httpBookUpdate.deleteOldBookmarkByEntry(list, handler, dbBookmarkKeys);
						this.httpBookUpdate.insertNewBookmark(list, presult);
					} catch(final Exception e) {
						log.fatal("DBLPUpdater: BookmarkUpdate: ", e);
					}
					list.clear();
				}
			}
		}
		count += list.size();
		if (list.size() > 1) { //store the rest
			try {
				httpBookUpdate.deleteOldBookmarkByEntry(list, handler, dbBookmarkKeys);
				httpBookUpdate.insertNewBookmark(list, presult);
			} catch(Exception e) {
				log.fatal("DBLPUpdater: BookmarkUpdate: ", e);
			}
		}
		return count;
	}

}