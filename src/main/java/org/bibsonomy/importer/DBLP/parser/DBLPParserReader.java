package org.bibsonomy.importer.DBLP.parser;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.zip.GZIPInputStream;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class DBLPParserReader {

	public static DBLPParseResult readDBLP(final String url) throws SAXException, IOException {
		// prepare parse xml
		final XMLReader xmlreader = XMLReaderFactory.createXMLReader();

		final DBLPParserHandler handler = new DBLPParserHandler();
		xmlreader.setContentHandler(handler);
		xmlreader.setEntityResolver(handler);
		xmlreader.setErrorHandler(handler);

		if (url != null && url.endsWith(".gz")) {
			final GZIPInputStream stream = new GZIPInputStream(new URL(url).openStream());
			final InputSource inputSource = new InputSource(stream);
			xmlreader.parse(inputSource);
		} else {
			xmlreader.parse(url);
		}
		return handler.getResult();
	}

}