package org.bibsonomy.importer.DBLP.parser;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bibsonomy.importer.DBLP.DBLPEvaluation;


/**
 * This class stores the results of the DBLP.xml parse
 */
public class DBLPParseResult {
	
	private final DBLPEvaluation eval = new DBLPEvaluation();

	/** new DBLP update date */
	private Date newDBLPdate;
	
	/** old DBLP update date */
	private Date dblpdate;
	
	/** all bibkeys */
	private final Set<String> allKeys = new HashSet<String>();

	/** all bibtex entries stored for each entry type */
	private final List<DBLPEntry> articles = new LinkedList<DBLPEntry>();

	private final List<DBLPEntry> inproceedings = new LinkedList<DBLPEntry>();

	private final List<DBLPEntry> proceedings = new LinkedList<DBLPEntry>();

	private final List<DBLPEntry> books = new LinkedList<DBLPEntry>();

	private final List<DBLPEntry> incollections = new LinkedList<DBLPEntry>();

	private final List<DBLPEntry> phdthesis = new LinkedList<DBLPEntry>();

	private final List<DBLPEntry> mastersthesis = new LinkedList<DBLPEntry>();

	private final List<DBLPEntry> data = new LinkedList<DBLPEntry>();
	
	private final List<DBLPEntry> www = new LinkedList<DBLPEntry>();

	/** stores all keys of crossreferenced entries with the reference and his dblpkey */
	private final Map<String, Map<String, DBLPEntry>> crossrefEntries = new HashMap<>();

	/** stores all crossreferenced entries */
	private final List<DBLPEntry> crossreflist = new LinkedList<DBLPEntry>();

	/** this list stores the four different types of errors */
	private final List<DBLPEntry> insert_incomplete_author_editor = new LinkedList<DBLPEntry>();

	private final List<DBLPEntry> insert_bookmark_empty_url = new LinkedList<DBLPEntry>();


	public Set<String> getAllKeys() {
		return allKeys;
	}

	public Date getDblpdate() {
		return dblpdate;
	}

	public void setDblpdate(Date dblpdate) {
		this.dblpdate = dblpdate;
	}

	public List<DBLPEntry> getArticles() {
		return articles;
	}

	public List<DBLPEntry> getBooks() {
		return books;
	}

	public List<DBLPEntry> getIncollections() {
		return incollections;
	}

	public List<DBLPEntry> getInproceedings() {
		return inproceedings;
	}

	public List<DBLPEntry> getMastersthesis() {
		return mastersthesis;
	}

	public List<DBLPEntry> getPhdthesis() {
		return phdthesis;
	}

	public List<DBLPEntry> getProceedings() {
		return proceedings;
	}
	
	public List<DBLPEntry> getData() {
		return data;
	}
	
	public List<DBLPEntry> getWww() {
		return www;
	}

	public Date getNewDBLPdate() {
		return newDBLPdate;
	}

	public void setNewDBLPdate(Date newDBLPdate) {
		this.newDBLPdate = newDBLPdate;
	}

	public List<DBLPEntry> getInsert_incomplete_author_editor() {
		return insert_incomplete_author_editor;
	}

	public List<DBLPEntry> getInsert_bookmark_empty_url() {
		return insert_bookmark_empty_url;
	}


	public Map<String, Map<String, DBLPEntry>> getCrossrefEntries() {
		return crossrefEntries;
	}

	public List<DBLPEntry> getCrossreflist() {
		return crossreflist;
	}

	public DBLPEvaluation getEval() {
		return eval;
	}
	
}