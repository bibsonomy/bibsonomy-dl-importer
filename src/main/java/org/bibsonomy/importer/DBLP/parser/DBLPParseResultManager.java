package org.bibsonomy.importer.DBLP.parser;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * this class checks the dblp entries and store them into DBLPParseResult
 */
public class DBLPParseResultManager{

	private static final Log log = LogFactory.getLog(DBLPParseResultManager.class);
	
	private DBLPParseResult result;

	
	public DBLPParseResultManager(){
		result = new DBLPParseResult();
	}

	public DBLPParseResult getResult() {
		return result;
	}

	public void setResult(DBLPParseResult result) {
		this.result = result;
	}
	
	public Date getNewDBLPdate() {
		return result.getNewDBLPdate();
	}

	public void setNewDBLPdate(Date newDBLPdate) {
		result.setNewDBLPdate(newDBLPdate);
	}
		
	public void setDblpdate(Date dblpdate){
		result.setDblpdate(dblpdate);
	}

	/**
	 * check if entry has crossref and store in crossrefEntries map
	 * @param entry
	 * @return
	 */
	public boolean checkCrossref(final DBLPEntry entry){
		if (entry.getCrossref() != null) {
			final Map<String, Map<String, DBLPEntry>> crossrefEntries = this.result.getCrossrefEntries();

			if (!crossrefEntries.keySet().contains(entry.getCrossref())) {
				/*
				 * node for this crossref does not exist --> create new node in crossref list
				 */
				crossrefEntries.put(entry.getCrossref(), new HashMap<String, DBLPEntry>());
			}
			/*
			 * add entry to crossref nodes entry list (crossrefEntries is an inverted index)
			 */
			crossrefEntries.get(entry.getCrossref()).put(entry.getDblpKey(), entry);
			return true;
		}
		return false;
	}
	
	/*
	 * this method checks if the entry has already a failure and store it in a failure or entrytype list
	 */
	public void addEntry(final DBLPEntry entry) {
		log.debug("adding entry " + entry);
		
		if (entry.getEntryType().equals(DBLPParserHandler.ENTRY_TYPE_ARTICLE)) {
			result.getEval().incArticleCount();
    		if (!checkCrossref(entry)) {
    			result.getArticles().add(entry);
    		}
    		result.getAllKeys().add(entry.getDblpKey());
		} else if (entry.getEntryType().equals(DBLPParserHandler.ENTRY_TYPE_INPROCEEDINGS)) {
			result.getEval().incInproceedingsCount();
			if (!checkCrossref(entry)) {
				result.getInproceedings().add(entry);
			}
			result.getAllKeys().add(entry.getDblpKey());
		} else if (entry.getEntryType().equals(DBLPParserHandler.ENTRY_TYPE_PROCEEDINGS)) {
			result.getEval().incProceedingsCount();
			if (!checkCrossref(entry)) {
				result.getProceedings().add(entry);
			}
			result.getAllKeys().add(entry.getDblpKey());
		} else if (entry.getEntryType().equals(DBLPParserHandler.ENTRY_TYPE_BOOK)) {
			result.getEval().incBookCount();
			if (!checkCrossref(entry)) {
				result.getBooks().add(entry);
			}
			result.getAllKeys().add(entry.getDblpKey());
		} else if (entry.getEntryType().equals(DBLPParserHandler.ENTRY_TYPE_INCOLLECTION)) {
			result.getEval().incIncollectionCount();
			if (!checkCrossref(entry)) {
				result.getIncollections().add(entry);
			}
			result.getAllKeys().add(entry.getDblpKey());
		} else if(entry.getEntryType().equals(DBLPParserHandler.ENTRY_TYPE_PHDTHESIS)){
			result.getEval().incPhdthesisCount();
			if (!checkCrossref(entry)) {
				result.getPhdthesis().add(entry);
			}
			result.getAllKeys().add(entry.getDblpKey());
		} else if(entry.getEntryType().equals(DBLPParserHandler.ENTRY_TYPE_MASTERSTHESIS)){
			result.getEval().incMastersthesisCount();
			if (!checkCrossref(entry)) {
				result.getMastersthesis().add(entry);
			}
			result.getAllKeys().add(entry.getDblpKey());
		} else if(entry.getEntryType().equals(DBLPParserHandler.ENTRY_TYPE_DATA)){
			result.getEval().incDataCount();
			if (!checkCrossref(entry)) {
				result.getData().add(entry);
			}
			result.getAllKeys().add(entry.getDblpKey());			
		} else if(entry.getEntryType().equals(DBLPParserHandler.ENTRY_TYPE_WWW)){
			result.getEval().incWwwCount();
			if (entry.getUrl() != null){
				result.getAllKeys().add(entry.getDblpKey());
				result.getWww().add(entry);
			} else { //empty url failure
				result.getEval().incInsert_bookmark_empty_url_count();
				result.getAllKeys().add(entry.getDblpKey());
				result.getInsert_bookmark_empty_url().add(entry);
			}
		}
	}
}