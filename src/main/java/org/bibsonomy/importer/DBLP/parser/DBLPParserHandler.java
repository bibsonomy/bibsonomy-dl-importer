package org.bibsonomy.importer.DBLP.parser;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/**
 * dblp.xml parse handler
 */
public class DBLPParserHandler extends DefaultHandler {

	private static final Log log = LogFactory.getLog(DBLPParserHandler.class);

	/*
	 * all entry types (currently) supported by DBLP
	 */
	public static final String ENTRY_TYPE_ARTICLE = "article";
	public static final String ENTRY_TYPE_INPROCEEDINGS = "inproceedings";
	public static final String ENTRY_TYPE_PROCEEDINGS = "proceedings";
	public static final String ENTRY_TYPE_BOOK = "book";
	public static final String ENTRY_TYPE_INCOLLECTION = "incollection";
	public static final String ENTRY_TYPE_PHDTHESIS = "phdthesis";
	public static final String ENTRY_TYPE_MASTERSTHESIS = "mastersthesis";
	public static final String ENTRY_TYPE_WWW = "www";
	public static final String ENTRY_TYPE_DATA = "data"; // added in 2024

	public static final HashSet<String> ENTRYTYPES = new HashSet<String>(Arrays.asList(
			ENTRY_TYPE_ARTICLE, 
			ENTRY_TYPE_INPROCEEDINGS, 
			ENTRY_TYPE_PROCEEDINGS, 
			ENTRY_TYPE_BOOK, 
			ENTRY_TYPE_INCOLLECTION, 
			ENTRY_TYPE_PHDTHESIS, 
			ENTRY_TYPE_MASTERSTHESIS, 
			ENTRY_TYPE_WWW,
			ENTRY_TYPE_DATA
			));

	public static final String FIELD_CHAPTER = "chapter";
	public static final String FIELD_SCHOOL = "school";
	public static final String FIELD_SERIES = "series";
	public static final String FIELD_ISBN = "isbn";
	public static final String FIELD_CROSSREF = "crossref";
	public static final String FIELD_NOTE = "note";
	public static final String FIELD_PUBLISHER = "publisher";
	public static final String FIELD_CITE = "cite";
	public static final String FIELD_CDROM = "cdrom";
	public static final String FIELD_EE = "ee";
	public static final String FIELD_URL = "url";
	public static final String FIELD_MONTH = "month";
	public static final String FIELD_NUMBER = "number";
	public static final String FIELD_VOLUME = "volume";
	public static final String FIELD_JOURNAL = "journal";
	public static final String FIELD_ADDRESS = "address";
	public static final String FIELD_YEAR = "year";
	public static final String FIELD_PAGES = "pages";
	public static final String FIELD_BOOKTITLE = "booktitle";
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_EDITOR = "editor";
	public static final String FIELD_AUTHOR = "author";

	public static final HashSet<String> ENTRYFIELDS = new HashSet<String>(Arrays.asList(FIELD_AUTHOR, FIELD_EDITOR, FIELD_TITLE, FIELD_BOOKTITLE,
			FIELD_PAGES,FIELD_YEAR,FIELD_ADDRESS, FIELD_JOURNAL, FIELD_VOLUME, FIELD_NUMBER, FIELD_MONTH, FIELD_URL,
			FIELD_EE, FIELD_CDROM, FIELD_CITE, FIELD_PUBLISHER, FIELD_NOTE, FIELD_CROSSREF, FIELD_ISBN, FIELD_SERIES,
			FIELD_SCHOOL, FIELD_CHAPTER));

	/** straight filled dblp entry */
	private DBLPEntry entry = null;

	/** results of parsing */
	private final DBLPParseResultManager manager;

	/** holds literals (i.e. the "real" data) */
	private StringBuffer buf = null;


	public DBLPParserHandler(){
		super();
		this.manager = new DBLPParseResultManager();
		this.manager.setNewDBLPdate(null);
	}

	/**
	 * parse start-tags
	 */
	@Override
	public void startElement (String uri, String name, String qName, Attributes atts) {
		if (name.equals("dblp")) {
			/*
			 * do nothing
			 */
			log.debug("found DBLP root node in XML");
		} else if (validEntryType(name)) {
			/*
			 * start of entry
			 */
			newEntry(name, atts);
		} else if (this.entry != null && validEntryField(name)) {
			/*
			 * entry field found
			 */
			buf = new StringBuffer();
		}
	}

	private void newEntry(final String name, final Attributes atts) {
		final String dateStr = atts.getValue("mdate");
		Date entrydate = null;
		try {
			entrydate = DBLPEntry.DATE_FORMAT.parse(dateStr);
		} catch (final ParseException e) {
			log.error("error parsing date " + dateStr, e);
		}
		if (entrydate != null) {
			// update, get only new or changed  entries
			if (manager.getNewDBLPdate() == null || entrydate.after(manager.getNewDBLPdate())) {
				// increases date for next update
				manager.setNewDBLPdate(entrydate);
			}

			int index = atts.getIndex("key");
			if (index != -1) { // key is set
				entry = new DBLPEntry();
				entry.setEntryType(name);
				entry.setDblpKey(atts.getValue(index));
				entry.setEntrydate(entrydate);
			} 
		}
	}

	/**
	 * parse end-tags
	 */
	@Override
	public void endElement (String uri, String name, String qName) {
		if (this.entry != null) {
			/*
			 * We are in an entry and, more specifically, at the end of ...
			 */
			if (validEntryField(name)) {
				/*
				 * ... one of the entry's fields.
				 */
				final String value = this.buf.toString();
				if (FIELD_AUTHOR.equals(name) || FIELD_EDITOR.equals(name)) {
					this.entry.setEntryFieldPerson(name, value);
				} else if (FIELD_URL.equals(name)) {
					this.entry.setUrl(value);
				} else {
					this.entry.setEntryField(name, value);
				}
			} else if (validEntryType(name)) {
				/*
				 * ... the entry itself. 
				 */
				manager.addEntry(entry);
				entry = null;
			}
		}
	}

	/**
	 * parse textdata between start and end-tag --> buffer it
	 */
	@Override
	public void characters (char ch[], int start, int length){
		buf.append(ch, start, length);
	}

	/*
	 * check if given string is a valid dblp entry type or not 
	 */
	private static boolean validEntryType(final String entryType) {
		return ENTRYTYPES.contains(entryType.toLowerCase());
	}

	/*
	 * check if given string is a valid dblp element or not 
	 */
	private static boolean validEntryField(String entry) {
		return ENTRYFIELDS.contains(entry.toLowerCase());
	}

	public DBLPParseResult getResult() {
		return manager.getResult();
	}

}