package org.bibsonomy.importer.DBLP.configuration;

/**
 * This class stores values about several items. This items are about the 
 * connectivity to the database and the download and store of the DBLP dataset.
 */
public class Configuration{
	
	/** the home url of the system (e.g.: http://localhost:8080/) */
	private String home;
	
	/** URL to DBLP dataset */
	private String url;
		
	/** the import user name */
	private String user = null;
	
	/** the cookie to authorize the user */
	private String cookie = null;
	
	/** DB host */
	private String dbhost = null;
	
	/** username for DB login */
	private String dbuser = null;
	
	/** database name */
	private String dbname = null;
	
	/** password for DB login */
	private String dbpassword = null;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		url = url.trim();
		if(!url.equals(""))
			this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDbhost() {
		return dbhost;
	}

	public void setDbhost(String dbhost) {
		this.dbhost = dbhost;
	}

	public String getDbpassword() {
		return dbpassword;
	}

	public void setDbpassword(String dbpassword) {
		this.dbpassword = dbpassword;
	}

	public String getDbuser() {
		return dbuser;
	}

	public void setDbuser(String dbuser) {
		this.dbuser = dbuser;
	}

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}
	
	public boolean isValid(){
		if (url != null && user != null && dbhost != null && dbuser != null && dbname != null) {
			return true;
		}
		return false;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getCookie() {
		return this.cookie;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	
}