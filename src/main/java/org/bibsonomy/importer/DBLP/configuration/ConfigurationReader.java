package org.bibsonomy.importer.DBLP.configuration;

import java.io.File;
import java.io.FileReader;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * this class prepare and starts the parse of the constants XML
 */
public class ConfigurationReader {
	
	public static Configuration readConfiguration(final File constantsXML) throws Exception{
		//prepare parse xml
		final XMLReader xmlreader = XMLReaderFactory.createXMLReader(); 	
		final ConfigurationHandler handler = new ConfigurationHandler();
		xmlreader.setContentHandler(handler);
		xmlreader.setErrorHandler(handler);

		final FileReader characterStream = new FileReader(constantsXML);
		// parse XML
		xmlreader.parse(new InputSource(characterStream));
	
		final Configuration conResult = handler.getConResult();
		characterStream.close();
		
		return conResult;
	}
	
}