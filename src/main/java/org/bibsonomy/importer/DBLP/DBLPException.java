package org.bibsonomy.importer.DBLP;

public class DBLPException extends Exception {
	private static final long serialVersionUID = 7737802527323630146L;

	public DBLPException(String message) {
		super(message);
	}

}
