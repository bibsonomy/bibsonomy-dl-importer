package org.bibsonomy.importer.DBLP;

import org.bibsonomy.importer.DBLP.parser.DBLPEntry;
import org.bibsonomy.importer.DBLP.parser.DBLPParserHandler;

/**
 * the evaluation class
 */
public class DBLPEvaluation {
	
	private int article_count = 0;
	private int inproceedings_count = 0;
	private int proceedings_count = 0;
	private int book_count = 0;
	private int incollection_count = 0;
	private int phdthesis_count = 0;
	private int mastersthesis_count = 0;
	private int data_count = 0;
	private int www_count = 0;

	private int update_article_count = 0;
	private int update_inproceedings_count = 0;
	private int update_proceedings_count = 0;
	private int update_book_count = 0;
	private int update_incollection_count = 0;
	private int update_phdthesis_count = 0;
	private int update_mastersthesis_count = 0;
	private int update_data_count = 0;
	private int update_www_count = 0;

	private int insert_bookmark_empty_url_count = 0;

	private int insert_duplicate_count = 0;

	private int exception_count = 0;
	
	private int insert_publications = 0;
	
	private int insert_bookmarks = 0;
		
	/*
	 * TODO: added by rja to count total
	 */
	private int entriesInInsertBibtexCalls = 0;

	public int incArticleCount() {
		return article_count++;
	}

	public int incBookCount() {
		return book_count++;
	}

	public int incIncollectionCount() {
		return incollection_count++;
	}

	public int incInproceedingsCount() {
		return inproceedings_count++;
	}

	public int incInsert_bookmark_empty_url_count() {
		return insert_bookmark_empty_url_count++;
	}

	public int incMastersthesisCount() {
		return mastersthesis_count++;
	}

	public int incPhdthesisCount() {
		return phdthesis_count++;
	}

	public int incProceedingsCount() {
		return proceedings_count++;
	}

	public int incDataCount() {
		return data_count++;
	}
	
	public int incWwwCount() {
		return www_count++;
	}
	
	public void incrementUpdate(DBLPEntry entry) {
		entriesInInsertBibtexCalls++; // TODO: added by rja */
		final String entryType = entry.getEntryType();
		if (entryType.equals(DBLPParserHandler.ENTRY_TYPE_ARTICLE)) {
			update_article_count++;
		} else if (entryType.equals(DBLPParserHandler.ENTRY_TYPE_INPROCEEDINGS)) {
			update_inproceedings_count++;
		} else if (entryType.equals(DBLPParserHandler.ENTRY_TYPE_PROCEEDINGS)) {
			update_proceedings_count++;
		} else if (entryType.equals(DBLPParserHandler.ENTRY_TYPE_BOOK)) {
			update_book_count++;
		} else if (entryType.equals(DBLPParserHandler.ENTRY_TYPE_INCOLLECTION)) {
			update_incollection_count++;
		} else if (entryType.equals(DBLPParserHandler.ENTRY_TYPE_PHDTHESIS)) {
			update_phdthesis_count++;
		} else if (entryType.equals(DBLPParserHandler.ENTRY_TYPE_MASTERSTHESIS)) {
			update_mastersthesis_count++;
		} else if (entryType.equals(DBLPParserHandler.ENTRY_TYPE_DATA)) {
			update_data_count++;
		} else if (entryType.equals(DBLPParserHandler.ENTRY_TYPE_WWW)) {
			update_www_count++;
		}
	}

	
	public void getParsedStats(final StringBuffer buf) {
		buf.append("article       = " + article_count + "\n");
		buf.append("inproceedings = " + inproceedings_count + "\n");
		buf.append("proceedings   = " + proceedings_count + "\n");
		buf.append("book          = " + book_count + "\n");
		buf.append("incollection  = " + incollection_count + "\n");
		buf.append("phdthesis     = " + phdthesis_count + "\n");
		buf.append("mastersthesis = " + mastersthesis_count + "\n");
		buf.append("data          = " + data_count + "\n");
		buf.append("www           = " + www_count + "\n");
	}
	
	public String eval(){
		final StringBuffer buf = new StringBuffer();
		buf.append("##############################\nEntries in insertBibtexCalls: " + entriesInInsertBibtexCalls + "############################\n");
		buf.append("parsed entrys: \n"); 
		buf.append("\n");
		getParsedStats(buf);
		buf.append("\n");
		buf.append("\n");
		buf.append("\n");
		buf.append("updated entrys: \n");
		buf.append("\n");
		buf.append("article       = " + update_article_count + "\n");
		buf.append("inproceedings = " + update_inproceedings_count + "\n");
		buf.append("proceedings   = " + update_proceedings_count + "\n");
		buf.append("book          = " + update_book_count + "\n");
		buf.append("incollection  = " + update_incollection_count + "\n");
		buf.append("phdthesis     = " + update_phdthesis_count + "\n");
		buf.append("mastersthesis = " + update_mastersthesis_count + "\n");
		buf.append("data          = " + update_data_count + "\n");
		buf.append("www           = " + update_www_count + "\n");
		buf.append("\n");
		buf.append("\n");
		buf.append("all publications = " + insert_publications + "\n");
		buf.append("all bookmarks    = " + insert_bookmarks + "\n");
		buf.append("\n");
		buf.append("\n");
		buf.append("\n");
		buf.append("failures: \n");
		buf.append("\n");
		buf.append("bookmarks with empty url field   = " + insert_bookmark_empty_url_count + "\n");
		buf.append("bibtex insert duplicate failures = " + insert_duplicate_count + "\n");
		buf.append("exceptions                       = " + exception_count + "\n");
		return buf.toString();
	}

}