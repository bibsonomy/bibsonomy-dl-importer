package org.bibsonomy.importer.DBLP;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.common.Pair;
import org.bibsonomy.importer.DBLP.configuration.Configuration;
import org.bibsonomy.importer.DBLP.configuration.ConfigurationReader;
import org.bibsonomy.importer.DBLP.db.DBHandler;
import org.bibsonomy.importer.DBLP.parser.DBLPParseResult;
import org.bibsonomy.importer.DBLP.parser.DBLPParserReader;
import org.bibsonomy.importer.DBLP.update.BibtexUpdate;
import org.bibsonomy.importer.DBLP.update.BookmarkUpdate;
import org.bibsonomy.importer.DBLP.update.CheckDeletedEntries;
import org.bibsonomy.importer.DBLP.update.HTTPBibtexUpdate;
import org.bibsonomy.importer.DBLP.update.HTTPBookmarkUpdate;
import org.bibsonomy.services.URLGenerator;
import org.bibsonomy.util.Sets;
import org.bibsonomy.util.StringUtils;

/**
 * main class for running the dblp updater
 */
public class DBLPUpdater {

	private static final Pattern CKEY_PATTERN = Pattern.compile(".*ckey\\s*=\\s*\"([0-9A-Fa-f]{32})\".*");

	protected static final Pattern USER_PATTERN = Pattern.compile(".*currUser\\s*=\\s*\"(.*?)\".*");

	static {
		// configure the logging
		System.setProperty("log4j.configuration", "dblp.log4j.properties");
	}

	private final static Log log = LogFactory.getLog(DBLPUpdater.class);


	public static void main(final String[] args) {
		if (args.length == 0) {
			System.err.println("Please specify a configuration file!");
			System.err.println("usage:");
			System.err.println("java -Xmx10000M " + DBLPUpdater.class.getName() + " DBLPConstants.xml");
			System.exit(1);
		}

		DBHandler dbhandler = null;

		/*
		 * big exception catching blog for ... apparently everything that could possibly go wrong.
		 */
		try {
			log.info("DBLP UPDATE started");
			log.info("reading configuration");
			final Configuration conf = ConfigurationReader.readConfiguration(new File(args[0]));

			/*
			 * get the information (ckey, session cookie)
			 * for executing requests against the webapp
			 */
			final String baseURL = conf.getHome();
			final String userCookie = conf.getCookie();
			final String userName = conf.getUser();
			log.info("getting credentials from webapp");
			final Pair<String, String> information = retrieveInformation(baseURL, userName, userCookie);

			final String cKey = information.getFirst();
			if (cKey == null) {
				throw new DBLPException("please update the user cookie");
			}

			final String sessionCookieValue = information.getSecond();
			if (sessionCookieValue == null) {
				throw new DBLPException("no session cookie found");
			}
			final String sessionCookie = "JSESSIONID=" + sessionCookieValue;

			log.info(String.format("session cookie: %s", sessionCookie));
			log.info(String.format("ckey: %s", cKey));

			/*
			 * read XML file from DBLP
			 */
			log.info("start reading XML");
			final DBLPParseResult data = DBLPParserReader.readDBLP(conf.getUrl());
			log.info("finished reading XML");
			final StringBuffer buf = new StringBuffer();
			data.getEval().getParsedStats(buf);
			log.info(buf);

			dbhandler = new DBHandler(conf); // does not much, just checks config validity (sort of)
			
			/*
			 * get old date from DB and update date to today
			 * FIXME: shouldn't we do this only on success?
			 */
			log.info("getting old DBLP date from database");
			dbhandler.open();
			final Date oldDate = dbhandler.getDBLPDate();
			data.setDblpdate(oldDate);
			final Date newDate = data.getNewDBLPdate();
			log.info("setting DBLP date from " + oldDate + " to " + newDate);
			dbhandler.setDBLPDate(newDate);
			dbhandler.close();

			log.info("initializing updaters");
			final HTTPBookmarkUpdate httpBookUpdate = new HTTPBookmarkUpdate(baseURL, userCookie, sessionCookie, cKey, userName);
			final HTTPBibtexUpdate httpBibUpdate = new HTTPBibtexUpdate(baseURL, userCookie, sessionCookie, cKey, userName);

			
			
			final BookmarkUpdate bookupdate = new BookmarkUpdate(httpBookUpdate, data, dbhandler);
			final BibtexUpdate bibupdate = new BibtexUpdate(httpBibUpdate, data, dbhandler);

			/*
			 * checking entries which must be deleted
			 */
			final CheckDeletedEntries cde = new CheckDeletedEntries(httpBookUpdate, httpBibUpdate);
			log.info("deleting old posts");
			cde.deleteOldPosts(data.getAllKeys(), dbhandler);

			/*
			 * do updates
			 */
			log.info("updating publications");
			int pcount = bibupdate.update(cde.getBibtexKeysInDatabase());
			log.info("updated " + pcount + " posts");

			log.info("updating bookmarks");
			int bcount = bookupdate.update(cde.getBookmarkKeysInDatabase());
			log.info("updated " + bcount + " posts");

			/*
			 * delete and insert DBLP home bookmark, because ist must stay in
			 * the first place
			 */
			log.info("updating dblp home page");
			httpBookUpdate.updateDBLPHome();

			log.info("DBLP Update finished");
			log.info(data.getEval().eval());

		} catch (final Exception e) {
			log.fatal("DBLPUpdater: ", e);
			if (dbhandler != null) 
				dbhandler.close();
		}
	}

	/**
	 * retrieves the specified cookie from the response
	 *
	 * @param connection
	 * @param cookieName
	 * @return
	 */
	protected static String getCookie(final HttpURLConnection connection, final String cookieName) {
		final Map<String, List<String>> headers = connection.getHeaderFields();
		final List<String> cookies = headers.get("set-cookie"); // FIXME: case-dependent!

		if (cookies == null) {
			return null;
		}

		for (final String cookie : cookies) {
			// a cookie has the form key=value; Path=/; HttpOnly
			final String key = cookieName + "=";
			if (cookie.startsWith(key)) {
				final String[] cookieValues = cookie.split("; ");
				final String keyAndValue = cookieValues[0];
				return keyAndValue.replaceFirst(key, "");
			}
		}

		// no key found return null
		return null;
	}

	protected static HttpURLConnection buildConnection(final String urlString, final Set<String> cookies) throws IOException {
		final String cookieString = StringUtils.implodeStringCollection(cookies, ";");

		final URL url = new URL(urlString);
		final HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
		urlConn.setAllowUserInteraction(false);
		urlConn.setDoInput(true);
		urlConn.setRequestProperty("Cookie", cookieString);
		urlConn.setDoOutput(false);
		urlConn.setUseCaches(false);
		urlConn.setInstanceFollowRedirects(false);
		return urlConn;
	}

	/**
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	protected static Pair<String, String> retrieveInformation(final String baseURL, final String userName, final String userCookie) throws IOException {
		final URLGenerator generator = new URLGenerator(baseURL);
		final String userHome = generator.getUserUrlByUserName(userName);
		log.info("getting ckey from " + userHome + " and checking if the user is logged in");

		/*
		 * configure connection
		 */
		final HttpURLConnection urlConn = buildConnection(baseURL, Sets.asSet(userCookie));
		urlConn.connect();

		/*
		 * get session from header
		 */
		final String sessionID = getCookie(urlConn, "JSESSIONID");

		log.debug("got HTTP response code " + urlConn.getResponseCode());
		log.debug(urlConn.getHeaderField("Location"));
		/*
		 * read result to extract ckey
		 */
		String ckey = null;
		final BufferedReader buf = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "UTF-8"));
		String line;

		boolean userIsLoggedIn = false;
		while ((line = buf.readLine()) != null) {
			log.debug(line);

			/*
			 * not loggedin users also get a ckey, so we check if we
			 * can find a ckey and a currUser that matches the configured
			 * user name
			 */
			if (line.contains("ckey")) {
				final Matcher matcher = CKEY_PATTERN.matcher(line);
				if (matcher.matches()) {
					ckey = matcher.group(1);
				}

				if (userIsLoggedIn) {
					break;
				}
			}

			if (line.contains("currUser")) {
				final Matcher matcher = USER_PATTERN.matcher(line);
				if (matcher.matches()) {
					final String loggedinUser = matcher.group(1);
					if (loggedinUser.equals(userName)) {
						userIsLoggedIn = true;
					}
				}

				if (ckey != null) {
					break;
				}
			}
		}

		urlConn.disconnect();
		buf.close();

		// cookie is invalid, please update
		if (!userIsLoggedIn) {
			return new Pair<>();
		}

		return new Pair<>(ckey, sessionID);
	}
}