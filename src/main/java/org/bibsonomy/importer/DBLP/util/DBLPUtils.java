package org.bibsonomy.importer.DBLP.util;

import java.util.regex.Pattern;

/**
 * @author dzo
 */
public class DBLPUtils {
	private static final Pattern NORM_PERSON_NAME_PATTERN = Pattern.compile(" [0-9]{4}$");
	
	/**
	 * @param value
	 * @return the normed person name
	 */
	public static String normPersonName(final String value) {
		if (value == null) {
			return value;
		}
		return NORM_PERSON_NAME_PATTERN.matcher(value).replaceAll("");
	}

}
