The following statements are neccessary to create a special dblp_update 
user for the database which is able to do the update (and nothing more):

grant SELECT on bibtex, user, bookmark, DBLP
grant INSERT on DBLP, DBLPFailures 

GRANT SELECT ON bibsonomy_clean.bibtex       TO 'dblp_update'@'gromit.kde.informatik.uni-kassel.de' IDENTIFIED BY 'Gib_Gas';
GRANT SELECT ON bibsonomy_clean.bookmark     TO 'dblp_update'@'gromit.kde.informatik.uni-kassel.de' IDENTIFIED BY 'Gib_Gas';
GRANT SELECT ON bibsonomy_clean.DBLP         TO 'dblp_update'@'gromit.kde.informatik.uni-kassel.de' IDENTIFIED BY 'Gib_Gas';
GRANT SELECT ON bibsonomy_clean.user         TO 'dblp_update'@'gromit.kde.informatik.uni-kassel.de' IDENTIFIED BY 'Gib_Gas';


GRANT INSERT ON bibsonomy_clean.DBLP         TO 'dblp_update'@'gromit.kde.informatik.uni-kassel.de' IDENTIFIED BY 'Gib_Gas';
GRANT INSERT ON bibsonomy_clean.DBLPFailures TO 'dblp_update'@'gromit.kde.informatik.uni-kassel.de' IDENTIFIED BY 'Gib_Gas';
